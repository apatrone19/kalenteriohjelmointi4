package com.kalenteri;

import com.kalenteri.calendar.ProposedTime;
import java.util.ArrayList;

/**
 * @author Juho Kantakoski
 */
public class Event {

    private String name;
    private String creator;
    private String length;
    private String repetition;
    private ArrayList<String> participants;
    private ArrayList<ProposedTime> proposedTimes;
    private ArrayList<String> pendingInvites;

    public Event(String name, String length, String repetition) {
        this.name = name;
        this.creator = "admin";
        this.length = length;
        this.repetition = repetition;
        this.participants = new ArrayList<>();
        participants.add("admin");
        proposedTimes = new ArrayList<>();
        this.pendingInvites = new ArrayList<>();
    }

    public Event(String name, String creator, String length, String repetition) {
        this.name = name;
        this.creator = creator;
        this.length = length;
        this.repetition = repetition;
        this.participants = new ArrayList<>();
        this.participants.add(creator);
        proposedTimes = new ArrayList<>();
        this.pendingInvites = new ArrayList<>();
    }

    public void addProposedTime(ProposedTime proposedTime) {
        proposedTimes.add(proposedTime);
    }

    public void removeProposedTime(ProposedTime proposedTime) {
        proposedTimes.remove(proposedTime);
    }

    public void setProposedTimes(ArrayList<ProposedTime> proposedTimes) {
        this.proposedTimes = proposedTimes;
    }

    public ArrayList<ProposedTime> getProposedTimes() {
        return this.proposedTimes;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getLength() {
        return this.length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public void addParticipant(String name) {
        this.participants.add(name);
    }

    public void removeParticipant(String name) {
        this.participants.remove(name);
    }

    public ArrayList<String> getParticipants() {
        return this.participants;
    }

    public void setParticipants(ArrayList<String> participants) {
        this.participants = participants;
    }

    public void addUserToPendingInvites(String username) {
        if (!this.pendingInvites.contains(username)) {
            this.pendingInvites.add(username);
        }
    }

    public ArrayList<String> getPendingInvites() {
        return this.pendingInvites;
    }

    public void consumePendingInvite(String username) {
        this.pendingInvites.remove(username);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
