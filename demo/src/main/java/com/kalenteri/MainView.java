/*
 * Created by JFormDesigner on Mon May 09 16:48:25 EEST 2022
 */

package com.kalenteri;

import java.awt.event.*;
import com.kalenteri.calendar.Calendar;
import com.kalenteri.calendar.ProposedTime;
import com.kalenteri.dialogs.EventCreationDialog;
import com.kalenteri.dialogs.InvitationDialog;
import com.kalenteri.dialogs.InviteUserDialog;
import com.kalenteri.dialogs.ParticipantListDialog;

import java.awt.*;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.*;

/**
 * @author Juho Kantakoski
 */
public class MainView extends JFrame {

    public MainView() {
        listModel = new DefaultListModel<>();
        initComponents();
    }

    private void initComponents() {
        calendar = new Calendar();

        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        leftPanel = new JPanel();
        eventListHorizontalSpacer = new JPanel(null);
        eventListScrollPane = new JScrollPane();
        eventJList = new JList();
        eventButtonSpacer = new JPanel(null);
        createEventButton = new JButton();
        rightPanel = new JPanel();
        calendarToolbarPanel = new JPanel();
        eventTitleLabel = new JLabel();
        eventLengthLabel = new JLabel();
        calendarToolbarSpacer = new JPanel(null);
        participantsButton = new JButton();
        toolbarButtonsSpacer = new JPanel(null);
        inviteButton = new JButton();

        //======== this ========
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Ajanehdotusapplikaatio-demo");
        setName("mainFrame");
        Container contentPane = getContentPane();
        contentPane.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));

        //======== leftPanel ========
        {
            leftPanel.setPreferredSize(new Dimension(170, 550));
            leftPanel.setMinimumSize(new Dimension(300, 550));
            leftPanel.setRequestFocusEnabled(false);
            leftPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

            //---- eventListHorizontalSpacer ----
            eventListHorizontalSpacer.setPreferredSize(new Dimension(10, 70));
            leftPanel.add(eventListHorizontalSpacer);

            //======== eventListScrollPane ========
            {
                eventListScrollPane.setPreferredSize(new Dimension(160, 375));

                //---- eventJList ----
                eventJList.setPreferredSize(new Dimension(150, 300));
                eventJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                eventListScrollPane.setViewportView(eventJList);
            }
            leftPanel.add(eventListScrollPane);

            //---- eventButtonSpacer ----
            eventButtonSpacer.setPreferredSize(new Dimension(15, 35));
            leftPanel.add(eventButtonSpacer);

            //---- createEventButton ----
            createEventButton.setText("Create new event");
            createEventButton.setPreferredSize(new Dimension(160, 50));
            createEventButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    createEventButtonClicked(e);
                }
            });
            leftPanel.add(createEventButton);
        }
        contentPane.add(leftPanel);

        //======== rightPanel ========
        {
            rightPanel.setPreferredSize(new Dimension(815, 550));
            rightPanel.setMinimumSize(new Dimension(685, 550));
            rightPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 5));

            //======== calendarToolbarPanel ========
            {
                calendarToolbarPanel.setPreferredSize(new Dimension(815, 70));
                calendarToolbarPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));

                //---- eventTitleLabel ----
                eventTitleLabel.setText("Event title JLabel");
                eventTitleLabel.setFont(eventTitleLabel.getFont().deriveFont(eventTitleLabel.getFont().getSize() + 10f));
                eventTitleLabel.setPreferredSize(new Dimension(250, 70));
                eventTitleLabel.setMaximumSize(new Dimension(300, 32));
                calendarToolbarPanel.add(eventTitleLabel);

                //---- eventLengthLabel ----
                eventLengthLabel.setText("lenLbl");
                eventLengthLabel.setPreferredSize(new Dimension(60, 20));
                calendarToolbarPanel.add(eventLengthLabel);

                //---- calendarToolbarSpacer ----
                calendarToolbarSpacer.setPreferredSize(new Dimension(230, 10));
                calendarToolbarPanel.add(calendarToolbarSpacer);

                //---- participantsButton ----
                participantsButton.setText("Participants");
                participantsButton.setActionCommand("ParticipantsButton");
                participantsButton.setPreferredSize(new Dimension(116, 45));
                participantsButton.addActionListener(e -> participantsButtonClicked(e));
                calendarToolbarPanel.add(participantsButton);

                //---- toolbarButtonsSpacer ----
                toolbarButtonsSpacer.setPreferredSize(new Dimension(20, 10));
                calendarToolbarPanel.add(toolbarButtonsSpacer);

                //---- inviteButton ----
                inviteButton.setText("Invite to event");
                inviteButton.setPreferredSize(new Dimension(132, 45));
                inviteButton.addActionListener(e -> inviteButtonClicked(e));
                calendarToolbarPanel.add(inviteButton);
            }
            rightPanel.add(calendarToolbarPanel);
        }
        contentPane.add(rightPanel);
        setSize(1000, 600);
        setLocationRelativeTo(null);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents

        eventJList.setModel(listModel);

        // Tapahtuman Example Event lisääminen näkymään ja muutaman käyttäjän sekä aikaehdotuksen lisäys
        listModel.addElement(new Event("Example event", "Example User", "1", "Monthly"));
        listModel.get(0).addParticipant("admin");
        listModel.get(0).addParticipant("User");
        listModel.get(0).addParticipant("User 2");
        listModel.get(0).addProposedTime(new ProposedTime(DayOfWeek.FRIDAY, LocalTime.of(18, 0),
                LocalTime.of(21, 0), "Example User", new Color(1f, 0f, 0f, 0.2f)));
        listModel.get(0).addProposedTime(new ProposedTime(DayOfWeek.FRIDAY, LocalTime.of(12, 15),
                LocalTime.of(16, 0), "User", new Color(0f, 0f, 1f, 0.2f)));
        listModel.get(0).addProposedTime(new ProposedTime(DayOfWeek.WEDNESDAY, LocalTime.of(9, 0),
                LocalTime.of(13, 0), "User 2", new Color(1f, 0f, 1f, 0.2f)));
        switchEvent(listModel.get(0));

        // Toinen esimerkkitapahtuma, johon käyttäjä on kutsuttu
        Event invitationExample = new Event("Example event 2", "Example user", "2", "Monthly");
        ArrayList<String> exampleParticipants = new ArrayList<>(Arrays.asList(
                "Example user",
                "Other user",
                "Other user 2",
                "Another user"
        ));
        invitationExample.setParticipants(exampleParticipants);
        invitationExample.addProposedTime(new ProposedTime(DayOfWeek.TUESDAY, LocalTime.of(9, 0),
                LocalTime.of(18, 30), "Other user", new Color(1f, 0.25f, 1f, 0.2f)));
        invitationExample.addUserToPendingInvites("admin");

        // Tapahtumalistan interaktion hoitaja
        eventJList.getSelectionModel().addListSelectionListener(e -> {
            if (e.getValueIsAdjusting()) return;
            switchEvent(eventJList.getSelectedValue());
        });

        calendar.setPreferredSize(new Dimension(810, 470));
        rightPanel.add(calendar);
        setResizable(false);
        setVisible(true);

        // Kutsun vastaanottamisen esimerkki
        InvitationDialog inviteExample = new InvitationDialog(invitationExample);
        Event inviteReturnValue = inviteExample.showDialog();
        if (inviteReturnValue != null)
            listModel.addElement(inviteReturnValue);
    }

    // Päivittää tapahtumaotsikon ja kalenterin näyttämään tapahtumaan liittyvät tiedot
    private void switchEvent(Event event) {
        eventTitleLabel.setText(event.getName());
        eventLengthLabel.setText("Length: " + event.getLength() + "h");
        calendar.setEvent(event);
    }

    // Uuden tapahtuman luonti
    private void createEventButtonClicked(MouseEvent e) {
        eventCreationDialog = new EventCreationDialog();
        // EventCreationDialogissa kutsuttu setModal(true), tämän kommentin jälkeinen koodi suoritetaan vasta
        // dialogista poistuttua
        Event createdEvent = eventCreationDialog.showDialog();
        if (createdEvent != null) listModel.addElement(createdEvent);
    }

    // Tapahtuman jäsenlistan näyttäminen
    private void participantsButtonClicked(ActionEvent e) {
        if (eventJList.getSelectedValue() == null) return;
        new ParticipantListDialog(eventJList.getSelectedValue().getName(),
                eventJList.getSelectedValue().getParticipants().toArray(new String[0]));
    }

    // Tapahtumaan kutsuminen
    private void inviteButtonClicked(ActionEvent e) {
        if (eventJList.getSelectedValue() == null) return;
        new InviteUserDialog(eventJList.getSelectedValue());
    }

    private EventCreationDialog eventCreationDialog;

    private Calendar calendar;
    private DefaultListModel<Event> listModel;

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel leftPanel;
    private JPanel eventListHorizontalSpacer;
    private JScrollPane eventListScrollPane;
    private JList<Event> eventJList;
    private JPanel eventButtonSpacer;
    private JButton createEventButton;
    private JPanel rightPanel;
    private JPanel calendarToolbarPanel;
    private JLabel eventTitleLabel;
    private JLabel eventLengthLabel;
    private JPanel calendarToolbarSpacer;
    private JButton participantsButton;
    private JPanel toolbarButtonsSpacer;
    private JButton inviteButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

}
