/*
 * Created by JFormDesigner on Sun Apr 17 14:15:40 EEST 2022
 */

package com.kalenteri.dialogs;

import com.kalenteri.Event;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * @author Juho Kantakoski
 */
public class InvitationDialog  {

    private Event eventInvitedTo;
    private boolean inviteAccepted;

    public InvitationDialog(Event event) {
        this.eventInvitedTo = event;
        initComponents();
    }

    public Event showDialog() {
        invitationDialogForm.setVisible(true);
        return inviteAccepted ? this.eventInvitedTo : null;
    }

    private void acceptButtonMouseClicked(MouseEvent e) {
        eventInvitedTo.consumePendingInvite("admin");
        eventInvitedTo.addParticipant("admin");
        inviteAccepted = true;
        invitationDialogForm.setVisible(false);
        invitationDialogForm.dispose();
    }

    private void declineButtonMouseClicked(MouseEvent e) {
        eventInvitedTo.consumePendingInvite("admin");
        inviteAccepted = false;
        invitationDialogForm.setVisible(false);
        invitationDialogForm.dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        invitationDialogForm = new JDialog();
        dialogPane = new JPanel();
        textPanel = new JPanel();
        invitationTextLabel = new JLabel();
        buttonPanel = new JPanel();
        leftSpacer = new JPanel(null);
        acceptButton = new JButton();
        middleSpacer = new JPanel(null);
        declineButton = new JButton();
        rightSpacer = new JPanel(null);

        //======== invitationDialogForm ========
        {
            invitationDialogForm.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            invitationDialogForm.setTitle("You have been invited to an event!");
            invitationDialogForm.setEnabled(false);
            invitationDialogForm.setResizable(false);
            Container invitationDialogFormContentPane = invitationDialogForm.getContentPane();
            invitationDialogFormContentPane.setLayout(new GridLayout());

            //======== dialogPane ========
            {
                dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
                dialogPane.setLayout(new GridLayout(2, 0));

                //======== textPanel ========
                {
                    textPanel.setLayout(new GridLayout());

                    //---- invitationTextLabel ----
                    invitationTextLabel.setText("You have been invited to {eventName}");
                    invitationTextLabel.setHorizontalAlignment(SwingConstants.CENTER);
                    textPanel.add(invitationTextLabel);
                }
                dialogPane.add(textPanel);

                //======== buttonPanel ========
                {
                    buttonPanel.setBorder(new EmptyBorder(12, 0, 0, 0));
                    buttonPanel.setLayout(new GridLayout(1, 2));
                    buttonPanel.add(leftSpacer);

                    //---- acceptButton ----
                    acceptButton.setText("Accept");
                    acceptButton.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            acceptButtonMouseClicked(e);
                        }
                    });
                    buttonPanel.add(acceptButton);
                    buttonPanel.add(middleSpacer);

                    //---- declineButton ----
                    declineButton.setText("Decline");
                    declineButton.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            declineButtonMouseClicked(e);
                        }
                    });
                    buttonPanel.add(declineButton);
                    buttonPanel.add(rightSpacer);
                }
                dialogPane.add(buttonPanel);
            }
            invitationDialogFormContentPane.add(dialogPane);
            invitationDialogForm.pack();
            invitationDialogForm.setLocationRelativeTo(invitationDialogForm.getOwner());
        }
        // JFormDesigner - End of component initialization  //GEN-END:initComponents

        invitationTextLabel.setText("You have been invited to " + eventInvitedTo.getName() + " by " +
                eventInvitedTo.getCreator() + ".");

        invitationDialogForm.setEnabled(true);
        invitationDialogForm.setModal(true);
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JDialog invitationDialogForm;
    private JPanel dialogPane;
    private JPanel textPanel;
    private JLabel invitationTextLabel;
    private JPanel buttonPanel;
    private JPanel leftSpacer;
    private JButton acceptButton;
    private JPanel middleSpacer;
    private JButton declineButton;
    private JPanel rightSpacer;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
