
package com.kalenteri.dialogs;

import com.kalenteri.calendar.ProposedTime;

import java.awt.Container;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;

import javax.swing.*;
/*
 * Created by JFormDesigner on Wed Apr 20 11:36:29 EEST 2022
 */
import javax.swing.text.MaskFormatter;


/**
 * @author Aleksi, Juho Kantakoski
 */
public class ChooseTimeDialog extends JDialog {

	private DayOfWeek day;
    private LocalTime startTime;
    private LocalTime endTime;
	private String creatorName;
	private int eventLength;
	private ProposedTime chosenTime;

	public ChooseTimeDialog(DayOfWeek day, LocalTime startTime, String creatorName, int eventLength)
	{
		this.day = day;
		this.startTime = startTime;
		this.creatorName = creatorName;
		this.eventLength = eventLength;
		initComponents();
	}

	private void buttonOKMouseClicked(MouseEvent e) {
		String startTimeString = startingTimeTextField.getText();
		String endTimeString = endingTimeTextField.getText();

		try {
			startTime =  LocalTime.parse(startTimeString);
			endTime = LocalTime.parse(endTimeString);
		} catch (DateTimeParseException ex) {
			JOptionPane.showMessageDialog(null, ex.getLocalizedMessage(), "Check your input!",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (startTime.isBefore(LocalTime.of(6, 0)) || endTime.isAfter(LocalTime.of(23, 0))
		|| startTime.isAfter(LocalTime.of(22, 59)) || endTime.isBefore(LocalTime.of(6, 1))) {
			JOptionPane.showMessageDialog(null, "The application only supports times between " +
					"6:00 and 23:00.", "Check your input!", JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		if (endTime.isBefore(startTime)) {
			JOptionPane.showMessageDialog(null, "The ending time can not be before the starting " +
					"time!", "Check your input!", JOptionPane.WARNING_MESSAGE);
			return;
		}

		LocalTime lengthOfProposal = endTime.minusHours(startTime.getHour());
		lengthOfProposal = lengthOfProposal.minusMinutes(startTime.getMinute());

		if (lengthOfProposal.compareTo(LocalTime.of(eventLength, 0)) < 0) {
			JOptionPane.showMessageDialog(null, "The suggested time needs to be longer or equal"
			+ " to the length of the event!", "You're going to need to make some more time",
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		chosenTime = new ProposedTime(day, startTime, endTime, creatorName);

		setVisible(false);
		dispose();
	}

	// Sulkee ikkunan kun käyttäjä painaa cancel
	private void buttonCancelMouseClicked(MouseEvent e) {
		setVisible(false);
		dispose();
	}

	public ProposedTime showDialog() {
		setVisible(true);
		return this.chosenTime;
	}

	private void initComponents() {

		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license - Aleksi
		chosenDayLabel = new JLabel();
		label2 = new JLabel();
		label3 = new JLabel();
		label4 = new JLabel();
		startingTimeTextField = new JFormattedTextField(createFormatter("##:##"));
		endingTimeTextField = new JFormattedTextField(createFormatter("##:##"));
		button1 = new JButton();
		button2 = new JButton();

		//======== this ========
		Container contentPane = getContentPane();

		//---- label1 ----
		chosenDayLabel.setText("Friday 13th of March");
		chosenDayLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));

		//---- label2 ----
		label2.setText("To");
		label2.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));

		//---- label3 ----
		label3.setText("From");
		label3.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));

		//---- label4 ----
		label4.setText("-");
		label4.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));

		//---- formattedTextField1 ----
		startingTimeTextField.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));
		// startingTimeTextField.setText("##:##");

		//---- formattedTextField2 ----
		endingTimeTextField.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));
		// endingTimeTextField.setText("##:##");

		//---- button1 ----
		button1.setText("OK");
		button1.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));
		button1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				buttonOKMouseClicked(e);
			}
		});

		//---- button2 ----
		button2.setText("Cancel");
		button2.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));
		button2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				buttonCancelMouseClicked(e);
			}
		});

		GroupLayout contentPaneLayout = new GroupLayout(contentPane);
		contentPane.setLayout(contentPaneLayout);
		contentPaneLayout.setHorizontalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addGap(107, 107, 107)
					.addComponent(chosenDayLabel, GroupLayout.PREFERRED_SIZE, 196, GroupLayout.PREFERRED_SIZE)
					.addGap(0, 115, Short.MAX_VALUE))
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addGroup(contentPaneLayout.createParallelGroup()
						.addGroup(contentPaneLayout.createSequentialGroup()
							.addGap(57, 57, 57)
							.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
								.addComponent(label3, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
								.addComponent(label2, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)))
						.addGroup(contentPaneLayout.createSequentialGroup()
							.addGap(27, 27, 27)
							.addComponent(button1, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addGroup(contentPaneLayout.createParallelGroup()
						.addComponent(endingTimeTextField, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)
						.addGroup(contentPaneLayout.createSequentialGroup()
							.addComponent(startingTimeTextField, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
							.addGroup(contentPaneLayout.createParallelGroup()
								.addComponent(button2, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
								.addComponent(label4, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(34, Short.MAX_VALUE))
		);
		contentPaneLayout.setVerticalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(chosenDayLabel, GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(label3, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
						.addComponent(startingTimeTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label4, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addGroup(contentPaneLayout.createParallelGroup()
						.addComponent(label2, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
						.addComponent(endingTimeTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(36, 36, 36)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(button2, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
						.addComponent(button1, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
					.addGap(19, 19, 19))
		);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents

		pack();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(getOwner());
		setModal(true);

		setTitle("Choose a preferred time for the event");

		chosenDayLabel.setText(day.toString());
		startingTimeTextField.setText(startTime.toString());
		endingTimeTextField.setText(startTime.plusHours(eventLength).toString());
		if (startTime.plusHours(eventLength).isAfter(LocalTime.of(23, 0))) {
			endingTimeTextField.setText("23:00");
		}
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license - Aleksi
	private JLabel chosenDayLabel;
	private JLabel label2;
	private JLabel label3;
	private JLabel label4;
	private JFormattedTextField startingTimeTextField;
	private JFormattedTextField endingTimeTextField;
	private JButton button1;
	private JButton button2;
	// JFormDesigner - End of variables declaration  //GEN-END:variables

	private MaskFormatter createFormatter(String format) {
		MaskFormatter formatter = null;
        try {
            formatter = new MaskFormatter(format);
        } catch (java.text.ParseException exc) {
            System.err.println("formatter is bad: " + exc.getMessage());
            System.exit(-1);
        }
        return formatter;
	}
}
