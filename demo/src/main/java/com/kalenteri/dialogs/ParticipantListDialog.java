package com.kalenteri.dialogs;


import java.awt.*;

import javax.swing.*;


/*
 * Created by JFormDesigner on Thu Apr 21 16:44:01 EEST 2022
 */



/**
 * @author Aleksi, Juho Kantakoski
 */
public class ParticipantListDialog extends JDialog {

	private String eventTitle;

	public ParticipantListDialog(String eventTitle, String [] participants) {
		this.eventTitle = eventTitle;
		initComponents(participants);
	}

    private void initComponents(String [] arr) {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license - Aleksi

		//esimerkki lista käyttäjistä
        //String [] Array_of_participants = {"me","Michael", "Christopher", "Jessica", "Matthew", "Ashley", "Jennifer", "Joshua", "Amanda", "Daniel", "David", "James", "Robert", "John", "Joseph", "Andrew", "Ryan", "Brandon", "Jason", "Justin", "Sarah", "William", "Jonathan", "Stephanie", "Brian", "Nicole", "Nicholas", "Anthony", "Heather", "Eric", "Elizabeth", "Adam", "Megan", "Melissa", "Kevin", "Steven", "Thomas", "Timothy", "Christina", "Kyle", "Rachel", "Laura", "Lauren", "Amber", "Brittany", "Danielle", "Richard", "Kimberly", "Jeffrey", "Amy", "Crystal"};
        scrollPane1 = new JScrollPane();
		Participants = new JList<String>(arr);


		//======== this ========
		setTitle("Participants of " + eventTitle);
		Container contentPane = getContentPane();

		//======== scrollPane1 ========
		{

			//---- Participants ----
			Participants.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 18));
			scrollPane1.setViewportView(Participants);
		}

		GroupLayout contentPaneLayout = new GroupLayout(contentPane);
		contentPane.setLayout(contentPaneLayout);
		contentPaneLayout.setHorizontalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 318, Short.MAX_VALUE)
					.addContainerGap())
		);
		contentPaneLayout.setVerticalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 296, Short.MAX_VALUE)
					.addContainerGap())
		);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
		pack();
		setResizable(false);    // aseteaan uudelleen koo'on asentaminen FALSE
		setDefaultCloseOperation(DISPOSE_ON_CLOSE); // Kun painaa X oikesta yläkulmasta niin dialogi sulkeutuu
		setLocationRelativeTo(getOwner());
		setModal(true);
		setVisible(true); // Asetetaan näkyvyys
	}


    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JScrollPane scrollPane1;
	private JList<String> Participants;
	// JFormDesigner - End of variables declaration  //GEN-END:variables



}

    

