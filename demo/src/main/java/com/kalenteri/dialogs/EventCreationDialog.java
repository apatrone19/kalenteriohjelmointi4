package com.kalenteri.dialogs;

import com.kalenteri.Event;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.text.MaskFormatter;


/*
* @auth Aleksi Patronen, Juho Kantakoski
*/
public class EventCreationDialog extends JDialog implements ActionListener {

    public EventCreationDialog() {
        initComponents();
    }

    private Event newEvent;

    private JTextField eventName_textField;
    private JTextField eventLength_textField;
    private JButton cancel_button;
    private JButton OK_button;
    private JComboBox<String> repetition_comboBox;
    private String [] repeat_options = {"Daily", "Weekly", "Monthly"};

    private void initComponents(){
        
        // LABEL EVENT NAME
        JLabel event_name = new JLabel(); // tehdään label event name kohdalle
        event_name.setText("Event name:"); // Asetetaan event name labliin teksti
        event_name.setFont(new Font("SansSerif",Font.PLAIN, 20)); // Asetetaan fontti ja koko
        event_name.setSize(30, 30);
        
        // LABEL EVENT LENGTH
        JLabel event_length = new JLabel(); // tehdään label event length kohdalle
        event_length.setText("Event length:"); // Asetetaan event length labliin teksti
        event_length.setFont(new Font("SansSerif",Font.PLAIN, 20)); // Asetetaan fontti ja koko
        event_length.setSize(30, 30);
        
        // LABEL REPEAT
        JLabel event_repeat = new JLabel(); // tehdään label event length kohdalle
        event_repeat.setText("Repeat:"); // Asetetaan event length labliin teksti
        event_repeat.setFont(new Font("SansSerif",Font.PLAIN, 20)); // Asetetaan fontti ja koko
        event_repeat.setSize(30, 30);
        
        // TEXTFIELD FOR EVENT NAME
        eventName_textField = new JTextField(); // luodaan teksti kenttä
        eventName_textField.setPreferredSize(new Dimension(30,30)); // asetaan koko
        
        // teksti kenttä missä numero
        eventLength_textField = new JFormattedTextField(createFormatter("##")); // luodaan teksti kenttä
        eventLength_textField.setPreferredSize(new Dimension(30,30)); // asetaan koko
        
        // buttonit cancel ja OK    
        cancel_button = new JButton();
        cancel_button.setText("CANCEL");
        cancel_button.addActionListener(this);
        cancel_button.setBounds(500, 470, 130, 80); // Asetaan koko ja sijainti
        
        OK_button = new JButton();
        OK_button.setText("OK");
        OK_button.addActionListener(this);
        OK_button.setBounds(50, 470, 130, 80); // Asetaan koko ja sijainti

        //Drop down valikko tähän
        repetition_comboBox = new JComboBox<String>(repeat_options); // luodaan valikko
        repetition_comboBox.setSelectedIndex(0);
        repetition_comboBox.addActionListener(this); // lisätään actionListener

        // tehdään paneeli ja layout
        JPanel panel = new JPanel(); // paneeli
        GridLayout gb = new GridLayout(4, 2,0,20); 
        panel.setLayout(gb);
        panel.setBounds(150, 200, 350, 150);
        panel.add(event_name);
        panel.add(eventName_textField);
        panel.add(event_length);
        panel.add(eventLength_textField);
        panel.add(event_repeat);
        panel.add(repetition_comboBox);
        panel.setVisible(true);

        // FRAME
        this.setTitle("CREATE AN EVENT"); // asetetaan framille otsikko

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        this.setResizable(false); // estetään kirjautumis ruudun levittäminen
        this.setSize(700,600); // asetetaan koko
        this.setLayout(null);
        this.add(panel);
        this.add(cancel_button);
        this.add(OK_button);
        setModal(true);
    }
    protected MaskFormatter createFormatter(String s) {
        MaskFormatter formatter = null;
        try {
            formatter = new MaskFormatter(s);
        } catch (java.text.ParseException exc) {
            System.err.println("formatter is bad: " + exc.getMessage());
            System.exit(-1);
        }
        return formatter;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == OK_button){
            if (eventName_textField.getText().isEmpty() || eventLength_textField.getText().isEmpty()) return;

            try {
                if (Integer.parseInt(eventLength_textField.getText()) > 16) {
                    JOptionPane.showMessageDialog(getOwner(), "An event can not last more than 16 hours.",
                            "The event would be too long!", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                if (Integer.parseInt(eventLength_textField.getText()) < 1) {
                    JOptionPane.showMessageDialog(getOwner(), "An event has to have a duration of at least 1 hour.",
                            "The event would be too short!", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(getOwner(), "There was something seriously wrong with your input.",
                        "Check your input!", JOptionPane.QUESTION_MESSAGE);
            }

            newEvent = new Event(eventName_textField.getText(), eventLength_textField.getText(),
                    repetition_comboBox.getItemAt(repetition_comboBox.getSelectedIndex()));
            setVisible(false);
            dispose();
        }
        if(e.getSource() == cancel_button){
            setVisible(false);
            dispose();
        }
    }

    public Event showDialog() {
        setVisible(true);
        return this.newEvent;
    }
}
