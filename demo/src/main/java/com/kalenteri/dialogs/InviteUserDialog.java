package com.kalenteri.dialogs;


import com.kalenteri.Event;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.GroupLayout;
/*
 * Created by JFormDesigner on Thu Apr 21 16:11:27 EEST 2022
 */



/**
 * @author Aleksi, Juho Kantakoski
 */
public class InviteUserDialog extends JDialog {

	private Event event;

	public InviteUserDialog(Event event) {
		this.event = event;
		initComponents();
	}

	private void buttonInviteMouseClicked(MouseEvent e) {
		if (userToInviteTextField.getText().isEmpty()) {
			JOptionPane.showMessageDialog(getOwner(), "The input is empty!", "Check your input!",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		this.event.addUserToPendingInvites(userToInviteTextField.getText());
		setVisible(false);
		JOptionPane.showMessageDialog(getOwner(), "An invitation has been sent to the user.", "Invite sent",
				JOptionPane.INFORMATION_MESSAGE);
		dispose();
	}

	//sulkee ikkunan kun painaa cancel
	private void buttonCancelMouseClicked(MouseEvent e) {
		setVisible(false);
		dispose();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license - Aleksi
		label1 = new JLabel();
		userToInviteTextField = new JTextField();
		button1 = new JButton();
		button2 = new JButton();

		//======== this ========
		setTitle("Invite user to " + event.getName());
		Container contentPane = getContentPane();

		//---- label1 ----
		label1.setText("Username:");
		label1.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));

		//---- textField1 ----
		userToInviteTextField.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));

		//---- button1 ----
		button1.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));
		button1.setText("Invite");
		button1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				buttonInviteMouseClicked(e);
			}
		});

		//---- button2 ----
		button2.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));
		button2.setText("Cancel");
		button2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				buttonCancelMouseClicked(e);
			}
		});

		GroupLayout contentPaneLayout = new GroupLayout(contentPane);
		contentPane.setLayout(contentPaneLayout);
		contentPaneLayout.setHorizontalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addGap(71, 71, 71)
					.addGroup(contentPaneLayout.createParallelGroup()
						.addComponent(label1, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
						.addComponent(button1, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
						.addComponent(userToInviteTextField, GroupLayout.PREFERRED_SIZE, 191, GroupLayout.PREFERRED_SIZE)
						.addComponent(button2, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(93, Short.MAX_VALUE))
		);
		contentPaneLayout.setVerticalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addGap(82, 82, 82)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(label1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
						.addComponent(userToInviteTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(51, 51, 51)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(button1)
						.addComponent(button2))
					.addContainerGap(45, Short.MAX_VALUE))
		);
        setResizable(false);
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
		setModal(true);
		setVisible(true);
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license - Aleksi
	private JLabel label1;
	private JTextField userToInviteTextField;
	private JButton button1;
	private JButton button2;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
