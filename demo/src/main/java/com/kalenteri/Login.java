package com.kalenteri;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.*;


/*
* @auth Aleksi Patronen
*/

public class Login extends JFrame implements ActionListener{
    JButton button_Logging;
    JButton button_Cancel;
    JTextField teksti_kayttajanimi;
    JTextField  teksti_salasanalle;
    String username;
    String password;
    
    Login()
    {
        // USERNAME LABE
        JLabel label_username = new JLabel(); // tehdään label usernamelle
        label_username.setText("Username:"); // Asetetaan lable_username labliin teksti
        label_username.setHorizontalAlignment(JLabel.CENTER); // horisontaalisuus
        label_username.setVerticalAlignment(JLabel.CENTER); // Vertikaalisuus
        label_username.setFont(new Font("SansSerif",Font.PLAIN, 20)); // Asetetaan fontti ja koko
        label_username.setBounds(80 , 100, 130, 150); // asetetaan labelin koko ja sijainti
        
        // PASSWORD LABEL
        JLabel label_password = new JLabel(); // tehdään label usernamelle
        label_password.setText("Password:"); // Asetetaan lable_username labliin teksti
        label_password.setHorizontalAlignment(JLabel.CENTER); // horisontaalisuus
        label_password.setVerticalAlignment(JLabel.CENTER); // Vertikaalisuus
        label_password.setFont(new Font("SansSerif",Font.PLAIN, 20)); // Asetetaan fontti ja koko
        label_password.setBounds(80 , 200, 130, 150); // asetetaan labelin koko ja sijainti
        
        // BUTTON logging
        button_Logging = new JButton(); //luodaan nappi
        button_Logging.setText("Logging"); // Asetetaan teksti
        button_Logging.addActionListener(this); // Asetataan buttonille action listener
        button_Logging.setBounds(80, 350, 100, 50); // Asetaan koko ja sijainti
        
        
        // BUTTON Cancel
        button_Cancel = new JButton(); //luodaan nappi
        button_Cancel.setText("Cancel"); // Asetetaan teksti
        button_Cancel.addActionListener(this); // Asetataan buttonille action listener
        button_Cancel.setBounds(300, 350, 100, 50); // Asetaan koko ja sijainti
        
        // TEXTFIELD FOR USERNAME
        teksti_kayttajanimi = new JTextField(); // luodaan teksti kenttä
        teksti_kayttajanimi.setPreferredSize(new Dimension(150,30)); // asetaan koko
        teksti_kayttajanimi.setBounds(200, 155, 200, 40); // aseteaab koko ja paikka
        
        // TEXTFIELD FOR PASSWORD
        teksti_salasanalle = new JPasswordField();          // luodaan teksti kenttä
        teksti_salasanalle.setPreferredSize(new Dimension(150,30)); // asetaan koko
        teksti_salasanalle.setBounds(200, 255, 200, 40);            // aseteaan koko ja paikka

        // FRAME
        //JFrame frame = new JFrame(); // luodaan fraimi
        this.setTitle("Logging"); // asetetaan framille otsikko
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // sulkee koko ohjelman kuin painaa X
        this.setResizable(false); // estetään kirjautumis ruudun levittäminen
        this.setSize(500,500); // asetetaan koko
        this.setLayout(null); // layout manager on NULL
        this.setVisible(true); // näkyvyys = TOSI
        this.add(label_username); // Lisätään label username
        this.add(label_password); // Lisätään label password
        this.add(button_Logging); //lisätään button logging nappi
        this.add(button_Cancel); // Lisätään button cancel nappi
        this.add(teksti_kayttajanimi); // lisätään teksti laatikko käyttäjäni  melle
        this.add(teksti_salasanalle); // lisätään teksti laatikko salasanalle


    }
    //Käyttäjä nimen ja salasanan tarkistus. Ja nappi actionlistener 
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == button_Logging){
            username = teksti_kayttajanimi.getText();
            password = teksti_salasanalle.getText(); 
            if(username.equals("admin") && password.equals("admin")){
                System.out.println("Käyttäjä nimi ja salasana oikein"); // Printataan kun salasana ja käyttäjätunnus oikein
                setVisible(false);
                dispose();
                new MainView();
                //this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING)); // suljetaan ikkuna kun käyttäjätiedot oikein
            } else {
                System.out.println("Käyttäjänimi tai salasana  väärin!"); // printataan kehotus siitä että käyttäjä tiedot väärin
                JOptionPane.showMessageDialog(this, "Invalid username or password!",
                        "Authentication failed", JOptionPane.ERROR_MESSAGE);
            }
        }
        if(e.getSource() == button_Cancel){
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING)); // suljetaan ikkuna kun painetaan CANCEL
        }
        
    }
    
}
