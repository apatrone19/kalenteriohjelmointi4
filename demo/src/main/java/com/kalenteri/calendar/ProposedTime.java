package com.kalenteri.calendar;

import java.awt.*;
import java.time.DayOfWeek;
import java.time.LocalTime;

/**
 * @author Juho Kantakoski
 */
public class ProposedTime {

    private DayOfWeek day;
    private LocalTime startTime;
    private LocalTime endTime;
    private String proposerName;
    private Color color = new Color(0f, 1f, 0f, 0.2f);

    public ProposedTime(DayOfWeek day, LocalTime startTime, LocalTime endTime, String proposerName) {
        this.day = day;
        this.startTime = startTime;
        this.endTime = endTime;
        this.proposerName = proposerName;
    }

    public ProposedTime(DayOfWeek day, LocalTime startTime, LocalTime endTime, String proposerName, Color color) {
        this.day = day;
        this.startTime = startTime;
        this.endTime = endTime;
        this.proposerName = proposerName;
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public DayOfWeek getDay() {
        return day;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public String getProposerName() {
        return proposerName;
    }
}
