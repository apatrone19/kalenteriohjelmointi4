package com.kalenteri.calendar;

import com.kalenteri.Event;
import com.kalenteri.dialogs.ChooseTimeDialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Juho Kantakoski
 */
public class Calendar extends JComponent {

    private Graphics2D g2;

    // Pikselimääriä
    private final int APPROX_LETTER_SIZE = 6;
    private final int DAY_HEADER_Y_SIZE = 50;
    private final int HOURS_SECTION_X_SIZE = 60;
    private final int DAY_SEPERATOR_X_MULTIPLIER = 107;
    private final int HOURS_SEPERATOR_BASE = 53;
    private final int HOURS_SEPERATOR_MULTIPLIER = 23;

    private final ArrayList<DayOfWeek> weekDays = new ArrayList<>();
    private final ArrayList<String> hoursToDraw = new ArrayList<>();

    private ChooseTimeDialog chooseTimeDialog;

    private Event event;

    public void setEvent(Event event) {
        this.event = event;
        repaint();
    }

    public Calendar() {
        weekDays.addAll(Arrays.asList(DayOfWeek.values()));
        for (int i = 6; i <= 23; i++) {
            hoursToDraw.add(i + ":00");
        }
        addMouseListener();
    }

    public Calendar(ArrayList<ProposedTime> proposedTimes) {
        weekDays.addAll(Arrays.asList(DayOfWeek.values()));
        for (int i = 6; i <= 23; i++) {
            hoursToDraw.add(i + ":00");
        }
        addMouseListener();
    }

    // --- HIIRI-INTERAKTION FUNKTIOT ---

    // Jos hiiriosoittimella painettiin jotain kalenterin tuntisolua, käynnistää aikaehdotusdialogin. Aikaehdotuksen
    // aloitusajan oletusarvo on arvio kellonajasta, jota kalenterissa painettiin.
    private void addMouseListener() {
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                if (getClickedDay(e.getPoint()) == null || getClickedHour(e.getPoint()) == -1) return;

                LocalTime clickedTime = LocalTime.of(getClickedHour(e.getPoint()),
                        (int) (getClickedHourPercentage(e.getPoint()) * 60));

                chooseTimeDialog = new ChooseTimeDialog(getClickedDay(e.getPoint()), clickedTime, "admin",
                        Integer.parseInt(event.getLength()));
                ProposedTime proposedTime = chooseTimeDialog.showDialog();

                if (proposedTime != null)
                    event.addProposedTime(proposedTime);
            }
        });
    }

    // Sisään koordinaatit, ulos viikonpäivä jolle koordinaatit osuvat. Palauttaa null jos koordinaatit ovat kalenterin
    // solujen ulkopuolella.
    private DayOfWeek getClickedDay(Point p) {
        if (p.getY() < DAY_HEADER_Y_SIZE || p.getX() > getWidth() - HOURS_SECTION_X_SIZE) return null;

        for (int i = 1; i <= 7; i++) {
            if (i == 1) {
                if (p.getX() < i * DAY_SEPERATOR_X_MULTIPLIER)
                    return weekDays.get(0);
            } else {
                if (p.getX() >= (i - 1) * DAY_SEPERATOR_X_MULTIPLIER && p.getX() <= i * DAY_SEPERATOR_X_MULTIPLIER)
                    return weekDays.get(i - 1);
            }
        }
        return null;
    }

    // Sisään koordinaatit, ulos kalenterin koordinaateissa sijaitsevan tuntisolun arvo (6-23). Jos koordinaatit ovat
    // tuntisolujen ulkopuolella, palauttaa -1.
    private int getClickedHour(Point p) {
        if (p.getY() < DAY_HEADER_Y_SIZE || p.getX() > getWidth() - HOURS_SECTION_X_SIZE) return -1;

        for (int i = 1; i <= hoursToDraw.size(); i++) {
            if (i == 1) {
                if (p.getY() < 53 + HOURS_SEPERATOR_MULTIPLIER) {
                    String[] splitString = hoursToDraw.get(0).split(":");
                    return Integer.parseInt(splitString[0]);
                }
            } else {
                if (p.getY() >= 53 + (i - 1) * HOURS_SEPERATOR_MULTIPLIER
                        && p.getY() < 53 + i * HOURS_SEPERATOR_MULTIPLIER) {
                    String[] splitString = hoursToDraw.get(i - 1).split(":");
                    return Integer.parseInt(splitString[0]);
                }
            }
        }
        return -1;
    }

    /*
     *  Palauttaa karkean arvion painetun koordinaatin suhteesta vastaavan tuntisolun kokoon, arvoväli 0-1
     *  (-1 jos koordinaatti ei ollut tuntisolu).
     *
     *  Käytetään painetun koordinaatin muuttamisessa suhteellisen tarkkaan kellonaikaan (tunti ja minuutit).
     */
    private float getClickedHourPercentage(Point p) {
        String clickedHour = getClickedHour(p) + ":00";

        int hourCellPixels = HOURS_SEPERATOR_MULTIPLIER;
        double absoluteY = p.getY() - (HOURS_SEPERATOR_BASE + hoursToDraw.indexOf(clickedHour) * hourCellPixels);
        float clickedHourPercentage = (float) absoluteY / hourCellPixels;

        return clickedHourPercentage > 0 ? clickedHourPercentage : 0;
    }

    // --- GRAFIIKKA ---

    // Kalenterin koko: 810x470
    @Override
    protected void paintComponent(Graphics g) {
        g2 = (Graphics2D) g;

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // Valkoisen taustan piirto
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, getWidth(), getHeight());

        // Kalenterin komponenttien piirtofunktiot
        g2.setColor(Color.BLACK);
        setBorder(BorderFactory.createLineBorder(Color.black, 2));
        drawGrid();
        drawHeadings();
        drawHours();
        drawProposedTimes();
    }

    // Päivien tekstiotsikot
    private void drawHeadings() {
        g2.setColor(Color.DARK_GRAY);
        g2.setFont(new Font("Serif", Font.BOLD, 16));

        // Maanantai
        g2.drawString(String.valueOf(weekDays.get(0)),
                107 / 2 - String.valueOf(weekDays.get(0)).length() * APPROX_LETTER_SIZE, 30);

        // Tiistai
        g2.drawString(String.valueOf(weekDays.get(1)),
                3 * 107 / 2 - String.valueOf(weekDays.get(1)).length() * APPROX_LETTER_SIZE + 4, 30);

        // Keskiviikko
        g2.drawString(String.valueOf(weekDays.get(2)),
                 5 * 107 / 2 - String.valueOf(weekDays.get(2)).length() * APPROX_LETTER_SIZE + 4, 30);

        // Torstai
        g2.drawString(String.valueOf(weekDays.get(3)),
                7 * 107 / 2 - String.valueOf(weekDays.get(3)).length() * APPROX_LETTER_SIZE + 5, 30);

        // Perjantai
        g2.drawString(String.valueOf(weekDays.get(4)),
                9 * 107 / 2 - String.valueOf(weekDays.get(4)).length() * APPROX_LETTER_SIZE + 6, 30);

        // Lauantai
        g2.drawString(String.valueOf(weekDays.get(5)),
                11 * 107 / 2 - String.valueOf(weekDays.get(5)).length() * APPROX_LETTER_SIZE + 5, 30);

        // Sunnuntai
        g2.drawString(String.valueOf(weekDays.get(6)),
                13 * 107 / 2 - String.valueOf(weekDays.get(6)).length() * APPROX_LETTER_SIZE + 2, 30);

        g2.setColor(Color.BLACK);
    }

    // Ruudukko
    private void drawGrid() {
        g2.setStroke(new BasicStroke(2));

        // Päivien otsikoiden erottaja
        g2.drawLine(0, DAY_HEADER_Y_SIZE, getWidth(), DAY_HEADER_Y_SIZE);

        // Kellonaikojen erottaja
        g2.drawLine(getWidth() - HOURS_SECTION_X_SIZE, 0, getWidth() - HOURS_SECTION_X_SIZE, getHeight());

        // Päivien erottajat
        for (int i = 1; i <= 7; i++) {
            g2.drawLine(i * DAY_SEPERATOR_X_MULTIPLIER, 0, i * DAY_SEPERATOR_X_MULTIPLIER, getHeight());
        }

        // Tuntien erottajat
        g2.setStroke(new BasicStroke(1));
        g2.setColor(Color.LIGHT_GRAY);
        for (int i = 1; i <= hoursToDraw.size(); i++) {
            g2.drawLine(0, HOURS_SEPERATOR_BASE + i * HOURS_SEPERATOR_MULTIPLIER, getWidth(),
                    HOURS_SEPERATOR_BASE + i * HOURS_SEPERATOR_MULTIPLIER);
        }

        g2.setColor(Color.BLACK);
    }

    // Kellonajat
    private void drawHours() {
        g2.setFont(new Font("Arial", Font.PLAIN, 12));
        for (int i = 1; i <= hoursToDraw.size(); i++) {
            g2.drawString(hoursToDraw.get(i - 1), getWidth() - 44, 46 + i * HOURS_SEPERATOR_MULTIPLIER);
        }
    }

    // Aikaehdotukset
    private void drawProposedTimes() {
        for (ProposedTime time : event.getProposedTimes()) {
            LocalTime startTime = time.getStartTime();
            LocalTime endTime = time.getEndTime();
            String creator = time.getProposerName();
            DayOfWeek day = time.getDay();
            int hourCellPixels = HOURS_SEPERATOR_MULTIPLIER;

            int xStart = weekDays.indexOf(day) * DAY_SEPERATOR_X_MULTIPLIER;
            int xEnd = (weekDays.indexOf(day) + 1) * DAY_SEPERATOR_X_MULTIPLIER;

            int yStart = 4 + (DAY_HEADER_Y_SIZE + (startTime.getHour() - 6) * HOURS_SEPERATOR_MULTIPLIER +
                     Math.round((startTime.getMinute() / 59f) * hourCellPixels));
            int yEnd = 3 + (DAY_HEADER_Y_SIZE + (endTime.getHour() - 6) * HOURS_SEPERATOR_MULTIPLIER +
                      Math.round((endTime.getMinute() / 59f) * hourCellPixels));

            g2.setColor(time.getColor());
            g2.fillRect(xStart, yStart, xEnd - xStart, yEnd - yStart);

            g2.setColor(Color.BLACK);
            g2.setFont(new Font("Arial", Font.BOLD, 12));
            g2.drawString(creator, xStart + 5, yStart + 15);
        }
    }
}
